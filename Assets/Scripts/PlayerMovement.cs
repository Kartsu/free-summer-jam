using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D Body;
    public float JumpSpeed;
    public float WalkSpeed;
    public float ReducedMovementFactor;
    float horizontalInput = 0;
    float verticalInput = 0;
    Vector2 bottomOffset;
    public float collisionRadius;
    public LayerMask groundLayer;
    bool grounded;
    bool canDash;
    private float dashTime;
    public float dashSpeed;
    public float startDashTime;
    private Vector2 direction;
    bool dashing;

    // Start is called before the first frame update
    void Start()
    {
        Body = GetComponent<Rigidbody2D>();
        bottomOffset = new Vector2(0f, -.3f);
        dashing = false;
    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");
        grounded = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
        if (Input.GetButtonDown("Jump") && grounded && !dashing)
        {
            Body.velocity = new Vector2(Body.velocity.x, JumpSpeed);
        }
        if (grounded)
        {
            canDash = true;
        }
        //canDash = Physics2D.OverlapCircle((Vector2)transform.position + new Vector2(-(horizontalInput * .32f), 0) + new Vector2 (0, -(verticalInput * .32f)), collisionRadius, groundLayer);
        if (canDash && !dashing)
        {
            if (Input.GetKeyDown("x"))
            {
                dashing = true;
                direction = new Vector2(horizontalInput, verticalInput);
                Body.gravityScale = 0f;
                canDash = false;
            }
        }
    }

    private void FixedUpdate()
    {
        if (dashing)
        {
            if (dashTime <= 0)
            {
                Body.gravityScale = 1.5f;
                direction = Vector2.zero;
                dashing = false;
                dashTime = startDashTime;
                Body.velocity = Vector2.zero;
            }
            else
            {
                dashTime -= Time.deltaTime;
                Body.velocity = direction * dashSpeed;
            }
        }
        else
        {
            MoveHorizontallyFromInput();
        }
    }

    protected void MoveHorizontallyFromInput(bool reduceMovement = false)
    {
        var targetVelocity = new Vector2(horizontalInput * WalkSpeed, Body.velocity.y);
        if (reduceMovement) Body.velocity = Vector2.Lerp(Body.velocity, targetVelocity, ReducedMovementFactor * Time.deltaTime);
        else Body.velocity = targetVelocity;
    }


}
